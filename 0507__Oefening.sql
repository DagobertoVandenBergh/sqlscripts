use modernways;

create table if not exists Metingen (
Tijdstip datetime not null,
Grootte smallint unsigned not null,
Marge decimal (3,2)
);