use modernways;

select Voornaam, Familienaam
from directieleden
having count(directieleden.Loon)  <= (select count(loon) from personeelsleden);