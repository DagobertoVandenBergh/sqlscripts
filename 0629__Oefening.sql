use modernways;

select studenten.id 
from studenten 
inner join evaluaties on studenten.id = evaluaties.Studenten_Id
group by studenten.id
having avg(Cijfer) > (select avg(Cijfer) from evaluaties)