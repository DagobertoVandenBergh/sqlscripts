use modernways;

create table if not exists Uitleningen
(
Boeken_Id int,
constraint fk_Uitleningen_Boeken foreign key(Boeken_Id) references Boeken(Id),
Leden_Id int,
constraint fk_Uitleningen_Leden foreign key(Leden_Id) references Leden(ID),
Startdatum date,
Einddatum date
);