use modernways;

select artiest, count(Aantalbeluisteringen) as aantal_beluisteringen
from modernways.liedjes
group by Artiest
having sum(Aantalbeluisteringen) >= 100;